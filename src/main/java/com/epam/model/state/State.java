package com.epam.model.state;

import com.epam.model.ConsoleColor;
import com.epam.model.TaskContext;
import com.epam.view.View;

public interface State {

    default void sprintBacklog(TaskContext context) {
        View.print("Unsupported state", ConsoleColor.RED);
    }

    default void inProgress(TaskContext context) {
        View.print("Unsupported state", ConsoleColor.RED);
    }

    default void peerReview(TaskContext context) {
        View.print("Unsupported state", ConsoleColor.RED);
    }

    default void inTest(TaskContext context) {
        View.print("Unsupported state", ConsoleColor.RED);
    }

    default void done(TaskContext context) {
        View.print("Unsupported state", ConsoleColor.RED);
    }

    default void blocked(TaskContext context) {
        View.print("Unsupported state", ConsoleColor.RED);
    }

}
