package com.epam.model.state.impl;

import com.epam.model.ConsoleColor;
import com.epam.model.TaskContext;
import com.epam.model.state.State;
import com.epam.view.View;

public class InTestState implements State {

    @Override
    public void inProgress(TaskContext context) {
        View.print("Task moved to In Progress state", ConsoleColor.GREEN);
        context.setState(new InProgressState());
    }

    @Override
    public void inTest(TaskContext context) {
        View.print("Task is already in In Test state", ConsoleColor.RED);
    }

    @Override
    public void done(TaskContext context) {
        View.print("Task moved to Done state", ConsoleColor.GREEN);
        context.setState(new DoneState());
    }

    @Override
    public void blocked(TaskContext context) {
        View.print("Task moved to Blocked state", ConsoleColor.GREEN);
        context.setState(new BlockedState());
    }

    @Override
    public String toString() {
        return "In Test";
    }

}
