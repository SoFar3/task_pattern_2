package com.epam.model.state.impl;

import com.epam.model.ConsoleColor;
import com.epam.model.TaskContext;
import com.epam.model.state.State;
import com.epam.view.View;

public class DoneState implements State {

    @Override
    public void done(TaskContext context) {
        View.print("Task is already in Done state", ConsoleColor.RED);
    }

    @Override
    public String toString() {
        return "Done";
    }

}
