package com.epam.model.state.impl;

import com.epam.model.ConsoleColor;
import com.epam.model.TaskContext;
import com.epam.model.state.State;
import com.epam.view.View;

public class SprintBacklogState implements State {

    @Override
    public void sprintBacklog(TaskContext context) {
        View.print("Task is already in Sprint Backlog state", ConsoleColor.RED);
    }

    @Override
    public void inProgress(TaskContext context) {
        View.print("Task moved to In Progress state", ConsoleColor.GREEN);
        context.setState(new InProgressState());
    }

    @Override
    public void blocked(TaskContext context) {
        View.print("Task moved to Blocked state", ConsoleColor.GREEN);
        context.setState(new BlockedState());
    }

    @Override
    public String toString() {
        return "Sprint Backlog";
    }

}
