package com.epam.model.state.impl;

import com.epam.model.ConsoleColor;
import com.epam.model.TaskContext;
import com.epam.model.state.State;
import com.epam.view.View;

public class ProductBacklogState implements State {

    @Override
    public void sprintBacklog(TaskContext context) {
        context.setState(new SprintBacklogState());
    }

    @Override
    public void blocked(TaskContext context) {
        View.print("Task moved to Blocked state", ConsoleColor.GREEN);
        context.setState(new BlockedState());
    }

    @Override
    public String toString() {
        return "Product Backlog";
    }

}
