package com.epam.model.state.impl;

import com.epam.model.ConsoleColor;
import com.epam.model.TaskContext;
import com.epam.model.state.State;
import com.epam.view.View;

public class PeerReviewState implements State {

    @Override
    public void inProgress(TaskContext context) {
        View.print("Task moved to In Progress state", ConsoleColor.GREEN);
        context.setState(new InProgressState());
    }

    @Override
    public void peerReview(TaskContext context) {
        View.print("Task is already in Peer Review", ConsoleColor.RED);
    }

    @Override
    public void inTest(TaskContext context) {
        View.print("Task moved to In Test state", ConsoleColor.GREEN);
        context.setState(new InTestState());
    }

    @Override
    public void blocked(TaskContext context) {
        View.print("Task moved to Blocked state", ConsoleColor.GREEN);
        context.setState(new BlockedState());
    }

    @Override
    public String toString() {
        return "Peer Review";
    }

}
