package com.epam.model.state.impl;

import com.epam.model.ConsoleColor;
import com.epam.model.TaskContext;
import com.epam.model.state.State;
import com.epam.view.View;

public class InProgressState implements State {

    @Override
    public void inProgress(TaskContext context) {
        View.print("Task is already in In Progress state", ConsoleColor.RED);
    }

    @Override
    public void peerReview(TaskContext context) {
        View.print("Task moved to Peer Review state", ConsoleColor.GREEN);
        context.setState(new PeerReviewState());
    }

    @Override
    public void blocked(TaskContext context) {
        View.print("Task moved to Blocked state", ConsoleColor.GREEN);
        context.setState(new BlockedState());
    }

    @Override
    public String toString() {
        return "In Progress";
    }

}
