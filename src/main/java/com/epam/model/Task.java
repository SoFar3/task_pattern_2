package com.epam.model;

import com.epam.model.state.State;

public class Task implements State {

    private int id;
    private String description;
    private TaskContext context;

    public Task(int id) {
        this(id, "Default task");
    }

    public Task(int id, String description) {
        this.id = id;
        this.description = description;
        this.context = new TaskContext();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskContext getContext() {
        return context;
    }

    public void setContext(TaskContext context) {
        this.context = context;
    }

    @Override
    public String toString() {
        return "Task: " +
                "id=" + id +
                ", description='" + description + '\'' +
                ", state=" + context;
    }

}
