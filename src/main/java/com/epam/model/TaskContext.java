package com.epam.model;

import com.epam.model.state.State;
import com.epam.model.state.impl.ProductBacklogState;

public class TaskContext {

    private State state;

    public TaskContext() {
        state = new ProductBacklogState();
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() { return state; }

    public void toSprintBacklog() {
        state.sprintBacklog(this);
    }

    public void toInProgress() {
        state.inProgress(this);
    }

    public void toPeerReview() {
        state.peerReview(this);
    }

    public void toInTest() {
        state.inTest(this);
    }

    public void toDone() {
        state.done(this);
    }

    public void toBlocked() {
        state.blocked(this);
    }

    @Override
    public String toString() {
        return state.toString();
    }

}
