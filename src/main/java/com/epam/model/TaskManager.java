package com.epam.model;

import com.epam.exception.TaskAlreadyExistsException;
import com.epam.exception.TaskDoesntNotExistsException;
import com.epam.view.View;

import java.util.ArrayList;
import java.util.List;

public class TaskManager {

    private List<Task> tasks;

    public TaskManager() {
        tasks = new ArrayList<>();
    }

    public void addTask(Task task) {
        if (tasks.stream().anyMatch(t -> t.getId() == task.getId())) {
            throw new TaskAlreadyExistsException();
        }
        tasks.add(task);
        View.print("+ task", ConsoleColor.GREEN_BOLD);
    }

    public Task getTask(int id) {
        Task task = tasks.stream().filter(t -> t.getId() == id).findFirst().orElse(null);

        if (task == null) {
            throw new TaskDoesntNotExistsException();
        }

        tasks.remove(task);

        View.print("- task", ConsoleColor.RED_BOLD);
        return task;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

}
