package com.epam.model;

import com.epam.exception.SprintEmptyException;
import com.epam.exception.SprintStartedException;
import com.epam.exception.TaskDoesntNotExistsException;
import com.epam.model.state.impl.DoneState;
import com.epam.model.state.impl.SprintBacklogState;
import com.epam.view.View;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Sprint {

    private List<Task> tasks;
    private boolean started = false;

    public Sprint() {
        tasks = new CopyOnWriteArrayList<>();
    }

    public void addTaskToSprint(Task task) {
        View.print("+ sprint task", ConsoleColor.GREEN_BOLD);

        if (started) {
            throw new SprintStartedException();
        }
        tasks.add(task);
    }

    public void start() {
        if (tasks.size() == 0) {
            throw new SprintEmptyException();
        }
        toSprintBacklog();
        started = true;
        View.print("Sprint started...", ConsoleColor.GREEN);
    }

    public void end() {
        tasks.stream()
                .filter(t -> ! (t.getContext().getState() instanceof DoneState))
                .forEach(t -> tasks.remove(t));
        started = false;
        View.print("Sprint ended...", ConsoleColor.GREEN);
    }

    public Task getTask(int id) {
        Task task = tasks.stream().filter(t -> t.getId() == id).findFirst().orElse(null);

        if (task == null) {
            throw new TaskDoesntNotExistsException();
        }

        return task;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    private void toSprintBacklog() {
        tasks.forEach(t -> t.getContext().setState(new SprintBacklogState()));
    }

}
