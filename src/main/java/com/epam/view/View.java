package com.epam.view;

import com.epam.model.ConsoleColor;
import com.epam.view.menu.Menu;

public interface View {

    static void print(String msg, String color) {
        System.out.print(color);
        System.out.println(msg);
        System.out.print(ConsoleColor.RESET);
    }

    void setMenu(Menu menu);

    void showMenu();

    String getString(String message);

    Integer getInteger(String message);

    void waitForUserOption();

    boolean doAction(String key);

}
