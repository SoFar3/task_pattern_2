package com.epam.controller;

import com.epam.exception.SprintEmptyException;
import com.epam.exception.TaskAlreadyExistsException;
import com.epam.exception.TaskDoesntNotExistsException;
import com.epam.model.ConsoleColor;
import com.epam.model.Sprint;
import com.epam.model.Task;
import com.epam.model.TaskManager;
import com.epam.model.state.impl.BlockedState;
import com.epam.model.state.impl.ProductBacklogState;
import com.epam.view.menu.Menu;
import com.epam.view.menu.Option;
import com.epam.view.View;
import com.epam.view.ViewImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ControllerImpl implements Controller {

    private View view;

    private TaskManager taskManager;
    private Sprint sprint;

    private static Logger logger = LogManager.getLogger();

    public ControllerImpl() {
        view = new ViewImpl();
        taskManager = new TaskManager();
        sprint = new Sprint();
    }

    @Override
    public void start() {
        Menu menu = new Menu(Integer.MAX_VALUE);

        mainMenu(menu);

        view.setMenu(menu);
        view.showMenu();
    }

    private void mainMenu(Menu menu) {
        menu.addMenuOption("1", new Option("Show all tasks", () -> {
            taskManager.getTasks().forEach(t -> View.print(t.toString(), ConsoleColor.YELLOW));
        }));

        menu.addMenuOption("2", new Option("Add new task", this::newTask));
        menu.addMenuOption("3", new Option("Add task to Sprint", this::addSprintTask));
        menu.getOption("3").setCondition(() -> ! sprint.isStarted());

        menu.addMenuOption("4", new Option("Start Sprint", () -> {
            try {
                sprint.start();
            } catch (SprintEmptyException e) {
                View.print("Sprint must not be empty...", ConsoleColor.RED);
            }
        }));
        menu.getOption("4").setCondition(() -> ! sprint.isStarted());

        menu.addMenuOption("5", new Option("To Sprint", () -> {
            Menu sprintMenu = new Menu(menu, Integer.MAX_VALUE);
            sprintMenu(sprintMenu);
            view.setMenu(sprintMenu);
        }));
        menu.getOption("5").setCondition(() -> sprint.isStarted());

        menu.addMenuOption("6", new Option("End Sprint", () -> {
            sprint.getTasks()
                    .stream()
                    .filter(t -> t.getContext().getState() instanceof BlockedState)
                    .forEach(t -> {
                        t.getContext().setState(new ProductBacklogState());
                        taskManager.addTask(t);
                    });
            sprint.end();
        }));
        menu.getOption("6").setCondition(() -> sprint.isStarted());

        menu.addMenuOption("7", new Option("INFO", () -> {
            View.print("Tasks in product backlog: " + taskManager.getTasks().size(), ConsoleColor.CYAN);
            View.print("Tasks in sprint: " + sprint.getTasks().size(), ConsoleColor.CYAN);
        }));

        menu.addMenuOption("Q", new Option("Quit", () -> System.exit(0)));
    }

    private void sprintMenu(Menu menu) {
        menu.addMenuOption("1", new Option("Show all tasks", () -> {
            sprint.getTasks().forEach(t -> View.print(t.toString(), ConsoleColor.YELLOW));
        }));

        menu.addMenuOption("2", new Option("Change task state", () -> {
            Menu stateMenu = new Menu(menu,1);
            stateMenu(stateMenu);
            view.setMenu(stateMenu);
        }));

        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));
    }

    private void stateMenu(Menu menu) {
        int taskId = 0;
        Task task = new Task(0);
        try {
            taskId = view.getInteger("Enter task id (int): ");
            task = sprint.getTask(taskId);
        } catch (TaskDoesntNotExistsException e) {
            View.print("Task with id: " + taskId + " doesn't exists...", ConsoleColor.RED);
            view.setMenu(menu.getParent());
        }

        Task finalTask = task;
        menu.addMenuOption("1", new Option("In Progress", () -> finalTask.getContext().toInProgress()));
        menu.addMenuOption("2", new Option("Peer Review", () -> finalTask.getContext().toPeerReview()));
        menu.addMenuOption("3", new Option("In Test", () -> finalTask.getContext().toInTest()));
        menu.addMenuOption("4", new Option("Done", () -> finalTask.getContext().toDone()));
        menu.addMenuOption("5", new Option("Blocked", () -> finalTask.getContext().toBlocked()));

        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));
    }

    private void addSprintTask() {
        int taskId = 0;
        while (true) {
            try {
                taskId = view.getInteger("Enter task id (int): ");
                sprint.addTaskToSprint(taskManager.getTask(taskId));
                break;
            } catch (TaskAlreadyExistsException e) {
                View.print("Task with id: " + taskId + " doesn't exists...", ConsoleColor.RED);
            }
        }
    }

    private void newTask() {
        Task task = new Task(0);
        while (true) {
            try {
                int taskId = view.getInteger("Enter task id (int): ");
                String description = view.getString("Enter a task description: ");
                task.setId(taskId);
                task.setDescription(description);
                taskManager.addTask(task);
                break;
            } catch (TaskAlreadyExistsException e) {
                View.print("Task with id: " + task.getId() + " already exists...", ConsoleColor.RED);
            }
        }
    }

}
